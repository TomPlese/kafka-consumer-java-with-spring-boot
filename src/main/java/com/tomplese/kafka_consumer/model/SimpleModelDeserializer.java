package com.tomplese.kafka_consumer.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;

public class SimpleModelDeserializer implements Deserializer<SimpleModel> {

    @Override
    public SimpleModel deserialize(String s, byte[] bytes) {

        SimpleModel message = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            message = mapper.readValue(bytes, SimpleModel.class);
        } catch (IOException e) {
        }
        return message;
    }



}
