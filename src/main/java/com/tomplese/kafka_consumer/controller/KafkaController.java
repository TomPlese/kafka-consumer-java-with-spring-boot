package com.tomplese.kafka_consumer.controller;

import com.tomplese.kafka_consumer.model.SimpleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaController {

    @KafkaListener(topics = "firstTopic")
    public void getFromKafka(SimpleModel simpleModel){
        System.out.println("New record: " + simpleModel.getFieldOne() + " ! " + simpleModel.getFieldTwo());
    }


}
